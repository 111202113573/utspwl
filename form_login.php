<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Toko Musik BenQQ</title>
    <!--CSS EKSTERNAL-->
    <link rel="stylesheet" href="form_login.css">
    
</head>

<body>
    <header class="header">
        <nav class="navcontainer">
            <a href="#" class="navBENQ">Toko Musik BenQQ</a>
        </nav>
    </header>
    <main>
        <section>
            <fieldset>
                <form method="post" action="index.php">
                    <h3>LOGIN</h3><br>
                    <div class="form">
                        <div >
                            <label>Username : </label>
                            <input type="text" name="uname" required> 
                        </div>
                        <br>
                        <div>
                            <label>Password : </label>
                            <input type="password" name="passwd" required>
                        </div>
                        <br>
                        <div>
                            <input type="submit" value="Login">
                        </div>
                        
                </form>
                <div class="php">
                    <?php
                      if(isset($_GET['message']))
                      {
                        if($_GET['message']=='nologin')
                        {
                          echo "Anda harus login terlebih dahulu<br>";
                        }
                        elseif($_GET['message']=='logout')
                        {
                          echo "Anda berhasil logout";
                        }
                        elseif($_GET['message']=="blabla")
                        {
                          echo "Anda menulis blabla";
                        }
                      }
                    ?>
                </div>
            </fieldset>
        </section>
    </main>
</body>
</html>
